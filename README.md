# 技术小组纳新试题

以下共有8题，你可以按照自己的情况选取若干题目完成，三至四题即可。其中第0题包含了题目解答上传的途径，因此必须完成。在解题过程中，你可以搜索相关资料，但请不要与其他人交流。

这些题目旨在了解各位的即时学习能力以及面向文档的学习能力，每道题目对于你来说都可能是完全陌生的，我们不需要你有多么优秀的基础，但我们欣赏你努力解题的过程。

如果在努力尝试后仍未能完全解答，可以谈一谈你对这些题目的理解，也可以将你对题目的探索过程作为附件提交，我们会酌情加分。

技术小组的纳新提交将于2023年10月12日23:59截止，逾期不候哦\~

**Have fun coding\~**

# 0. 来签到啦！- Git

作为一位（准）开发人员，你会遇到很多很多与他人合作完成项目的场景。你也许听说过“这个项目我Fork了”“这个项目我Star了”这样的说法，其实这都与Git息息相关。

Git 是一个开源的分布式版本控制系统，用于敏捷高效地处理任何或小或大的项目。它是 Linus Torvalds 为了帮助管理 Linux 内核开发而开发的一个开放源码的版本控制软件。

### 你的任务

1.  新建一个Git远程仓库，并保证技术小组管理员可以访问到你的仓库，托管平台不限。
2.  使用你的学号作为密钥，将管理员发送给你的一串密文进行解密，并将解密结果放在一个文本文件里，和其他题目的作答分别放在以题目序号命名的文件夹中，推送到你自己的远程仓库。文件格式不限。
3.  技术小组的纳新试题也会同步放在ZJU Git以及Gitea托管平台，当你完成时，请在纳新试题仓库提出issue，并附上你的个人仓库链接。

### 明文的加密过程

1.  明文固定4位，密钥固定学号后4位
2.  将明文与学号对应位置的字符先转化为整数，再相乘，将结果转换为16进制
3.  将4个独立的运算结果之间以`"/"`分割，再整合为一个字符串（密文）
4.  如果搞不清楚这个运算过程，下面有一个利用Python实现的加密解密示例：

```python
# 明文固定4位，密钥为你的学号后4位
encrypt = lambda clear, key: "".join([hex(ord(clear[i]) * ord(key[i]))[2:] + '/' for i in range(4)])[:-1]

decrypt = lambda cipher, key: "".join([chr(int(cipher.split('/')[i], 16) // ord(key[i])) for i in range(4)])
```

你可能会用到的Git命令：

-   `git init`：在当前目录初始化Git仓库
-   `git add . `：将当前目录下所有文件加入暂存区
-   `git commit -m "xxx"`：提交暂存区的更改，并附加评论“xxx”
-   `git push`：将本地仓库的更改推送到远程仓库
-   `git clone https://xxxx`：将\[URL]的仓库克隆到本地

### 附加题

-   请简单解释一下这两个函数是如何只用一行代码就完成工作的。它们还能再简化吗？

如果你实在无法通过Git将代码上传到你的个人仓库，请于2023年10月11日23:59之前打包发送到dawn\_ocean\@qq.com，邮件标题格式为 **姓名-学号-部门-技术小组纳新**。

### 参考资料

-   [Git](https://git-scm.com/ "Git")
-   [Learn Git Branching](https://learngitbranching.js.org/?locale=zh_CN "Learn Git Branching")
-   [ZJU Git](https://git.zju.edu.cn/ "ZJU Git")
-   [Python入门](https://www.runoob.com/python/python-tutorial.html "Python入门")

# 1. 原神，关闭！- RegExp

马老师最近有一个苦恼：只要看见白色的东西，脑袋里就会莫名奇妙的响起“原神，启动！”，甚至连跟朋友聊天时发送的文本也无法避免。他下定决心要改变这个现状，打算设计一个文字过滤器。这时，他想到了正则表达式。

正则表达式是程序开发中一种常见的处理文本的工具。正则表达式可以用来“匹配”文本,这种匹配关系
我们可以这样描述:

-   一个特定字符 **c**, 可以匹配其本身。例如： "E" 可以匹配 "E"，"志" 可以匹配 "志"。
-   **r∗**，这里的r 也是一个正则表达式，这个“星号构造”表示：可以匹配0 次、1 次或者多次r。例如："E\*" 可以匹配 "" (空字符串,匹配0 次)，"E" (匹配1 次)，"EEEEE" (匹配多次)。
-   **r?**，这里的r 也是一个正则表达式，这个“问号构造”表示：可以匹配0 次或者1 次r。例如："志?" 可以匹配 "" (空字符串,匹配0 次)，"志" (匹配1 次)。
-   &#x20;**r+**，这里的r 也是一个正则表达式，这个“加号构造”表示：可以匹配1 次或者多次r。例如："者+" 可以匹配 "者" (匹配1 次)，"者者者" (匹配多次)。
-   **r1|r2| . . . |rn**，这里的每一个r(i) 都是一个正则表达式，这个构造表示：选择匹配，匹配r1至rn 中的某一个。例如："E|志|者"，将会匹配 "E" 或者 "志" 或者 "者"。
-   **\[r1-rn]**，这里的r(i)可以是数字或者字母，和上一个构造类似，表示：匹配r1至rn中的某一个。例如："\[D-F]志者"可以匹配 "D志者"，"E志者"，"F志者"。
-   **r1r2 . . . rn**，这里的每个r(i) 都是一个正则表达式，这个构造表示：依次从左到右匹配r(i)。例如："E 志者" 可以匹配 "E 志者" 或者 "EE志志者"。
-   **.** ，是指一个点号 "."，它在正则表达式中可以匹配除了换行符"\n"以外的任意字符。
-   最后一种构造是括号构造，你可以通过加括号来改变上文中各种构造的范围。例如："E志者"，可以匹
    配 "E 志者者者"，以及"E 志者E 志者"；但 "(E 志者)" 只可以匹配 "E 志者E 志者"。再如，"E|e 志者" 只能匹配 "E" 或者 "e 志者"；而"(E|e) 志者" 则可以匹配 "E 志者"、"e 志者"。

### 你的任务

现在，请你使用以上规则设计一个正则表达式，来过滤掉马老师不希望看到的内容。满足以下要求的文本（文本不包含换行符）会被正则表达式匹配：

-   同时依次出现“白色”、“原神”、“启动”这三个元素；
-   “白色”元素：包含“白色”，或者以RGB形式表示：在文本中出现“#”号紧跟的6位十六进制数字（从左向右数第1、2位代表Red，3、4位代表Green，5、6位代表Blue，字母均大写），并且代表R/G/B的数字均大于等于FA、小于等于FF；
-   “原神”元素：包含“原神”、“OP(不区分大小写)”任一；
-   “启动”元素：包含“启”“动”这两个字，按顺序出现“启”“动”。

### 附加题

-   “启”字的个数为3的倍数。

例如，以下文本会被正则表达式匹配：

-   （#FAFBFC) 原神，启启启动！
-   白色，我超，居然是op！不管你启不启动，这下不得不启动了！

以下文本则不会被正则表达式匹配：

-   这不是我们原神的山里灵活吗，下次启启启动记得标明出处#FFACKU。
-   这次即使看到白色的东西，我都不可能启动了。（蓄力）Genshin\`，启动！你看，没有启动吧。

# 2. 巴别图书馆 - C\#

1941年，博尔赫斯在短篇小说《巴别图书馆》（Babel Library） 中，构想了一个图书馆式的宇宙：不管是已写成的还是永不被写的书，都会被收集在图书馆内。

也许你会联想到猴子和打字机：如果无数多的猴子在无数多的打字机上随机的打字，并持续无限久的时间，那么在某个时候，它们必然会打出莎士比亚的全部著作。

那么现在就让我们来扮演一次猴子，当然，是更加聪明的猴子。

请使用C# 语言，生成一个文本文件，它包含一段随机的文本，并符合以下要求：

-   仅包含大小写字母、（半角）逗号、句号和空格、换行符；
-   字符数为500～600；
-   整篇文本由若干个语句组成，语句以大写字母开头，以句号结尾，包含1～20个单词，设定1～2长度概率为10%，3～9长度概率为70%，10长度及以上概率为20%。在生成概率相同的语句中，要求各长度语句出现概率均等（如3～9长度每个长度有10%概率生成），各单词间可能会出现逗号（随机），不能出现连续的标点符号；
-   每个单词由1～20个字母组成，设定1～2长度概率为10%，3～9长度概率为70%，10长度及以上概率为20%。在生成概率相同的单词中，要求各长度单词出现概率均等。要求每个单词中必须包含元音部分，未位于句首则不大写；
    元音部分：\[a, e, i, o, u, y]
-   语句之间可能会出现分段（随机），在语句末尾添加换行符，并在下一个语句开头添加4个空格。文本中第一个段落不添加空格。
-   标点符号前不空格，后空一格。

最后，请给这个文件随机生成一个标题，符合以下要求：

-   由1～5个单词构成，每个单词首字母均大写
-   单词的要求与文本内一致

### 附加题

请编写一个C# 程序，分析某一个随机文本文件。输出如下：

```text
Title: Budfh Tsar //第一行：标题
Word Count: 556   //第二行：单词数
Sent Count: 50    //第三行：语句数
Para Count: 5     //第四行：段落数
EVA Appear: 6     //第五行：文本中出现"eva"的次数
Word Freq:
1. 23             //单词长度：频率数
2. 24
3. 52
...
20. 2
```

提交时，请将源代码和随机生成的三个文本文件一同打包。

参考资料：

巴别图书馆 - [https://libraryofbabel.info/](https://libraryofbabel.info/ "https://libraryofbabel.info/")

C#入门 - [https://www.runoob.com/csharp/csharp-tutorial.html](https://www.runoob.com/csharp/csharp-tutorial.html "https://www.runoob.com/csharp/csharp-tutorial.html")

[https://learn.microsoft.com/zh-cn/dotnet/csharp/tour-of-csharp/](https://learn.microsoft.com/zh-cn/dotnet/csharp/tour-of-csharp/ "https://learn.microsoft.com/zh-cn/dotnet/csharp/tour-of-csharp/")

# 3. 我来造轮子 - Bash

编写一个bash脚本`search_file.sh`，要求：

-   要求用户输入要搜索的扩展名和要搜索的目录。当用户输入q，程序退出

> 如：

```text
Please input file extension (q to quit): 
Please input directory to search (q to quit):
```

-   根据用户输入的扩展名和目录，递归搜索所有扩展名匹配的文件，将其路径输出到屏幕（绝对/相对路径都可以）。
-   若文件路径是绝对路径，且以家目录开头，将整个家目录替换成`~`

> 注意：[环境变量](https://zhuanlan.zhihu.com/p/557885534 "环境变量")`$HOME`就是家目录的绝对路径。在终端中，可以用`echo $HOME`查看家目录的绝对路径。在bash脚本中，同样使用`$HOME`来指代家目录的绝对路径。

```text
$ echo $HOME
/c/Users/<user name>     # git bash
/home/<user name>        # linux
```

> 示例：将以家目录开头的目录替换为\~开头：

```text
/home/fracher/code/test.c   ->   ~/code/test.c
```

-   输出一共有多少个文件
-   从第一步开始重复，直到用户输入q

### 附加要求

-   使输出更加好看，可以在echo时为文本[添加颜色](https://zhuanlan.zhihu.com/p/181609730 "添加颜色")。
-   此脚本不符合Unix哲学，即一件工具只做一件事。要求使用参数来控制该脚本的行为。仅当参数为`--interactive`时，才进行上面的轮询，否则仅根据参数进行一次搜索。如：
    ```bash
    $ ./search_file.sh --extension docx --directory ./Documents
    ./Documents/file1.docx
    ./Documents/file2.docx
    Total 2 files.
    $ ./search_file.sh --interactive
    Please input file extension (q to quit): ...
    Please input directory to search (q to quit): ...
    ...
    $ ./search_file.sh --help               # 输出帮助信息
    search_file.sh is a script to search files in a particular directory.
    USAGE:
    search_file.sh [OPTIONS]
    ... ... ...

    ```
    注：帮助信息的样例可以参考系统中的命令的帮助信息，如：
    ```bash
    $ ls --help
    $ cp --help
    ```
    简单写一点即可，不必过于详细。

### 可以参考的项目

-   Fracher的 [配置文件管理器](https://github.com/FrozenArcher/dotfiles/blob/main/dot "配置文件管理器")
-   [i3lock-fancy](https://github.com/meskarune/i3lock-fancy/blob/master/i3lock-fancy "i3lock-fancy")

### 另编

Vim之父 Bram Moolenaar 于2023年8月3日去世。让我们深切悼念这位开发者，他为自己的人生写下了最后一句 ":wq"。

# 4. 万能的F12 - Chrome Dev Tool

题目出自ACTF2020 Game1。

请在校网环境下访问[http://10.214.160.131/](http://10.214.160.131/ "http://10.214.160.131/")这个链接，在完成相应的题目后拿到一串代表你已经成功做完题目的FLAG，如ACTF{xxxxxxxxxxxxxx}。

在这个题目中，你需要自己学会如何最简单的使用Chrome 开发者工具，用它来观察、思考出题人的本
意。在之后的内训过程中，我们也会涉及如何使用好这个无敌的工具，但是现在，我们希望你能靠自己完
成。

这里是一个比较精简的介绍:
[https://www.cnblogs.com/constantince/p/4565261.html](https://www.cnblogs.com/constantince/p/4565261.html "https://www.cnblogs.com/constantince/p/4565261.html")

# 5. 网页小实践 - Frontend

请制作一个本地网页，实现一些简单的效果。要求如下：

> 功能要求：

-   这个网页有一个文本输入框和一个按钮，按钮上写着“生成”字样
-   在文本框中输入一个数字，按下“生成”按钮，下方会生成对应数量的小方块，小方块从左至右，从上至下以一定间距排列
-   左键点击小方块可以消除该小方块，其他小方块顺位排列填补空缺
-   在已经有生成小方块的情况下点击“生成”按钮，会在原有基础上增加相应数量的小方块

> 外观样式要求：

-   没有硬性要求，当然我们鼓励你按自己想法装饰网页

> 文件组织要求：

-   采用外置的JavaScript脚本和CSS层叠样式表
-   最终应该提交一个文件夹，内含一个.html文件，.css文件，一个.js文件。

***

样例：

1.  网页刚刚加载&#x20;

![](image/p1_YDG9sgo3rv.png)

1.  在文本框中输入"3"并点击"生成"按钮

![](image/p2_d8lLSjKqx0.png)

1.  左键单击最左侧的小方块&#x20;

![](image/p3_zTbicgK-TB.png)

1.  再次点击"生成"按钮

![](image/p4_Yd9bfVwRC0.png)

***

一些可能有助于完成项目的小提示：

-   小方块的长相和排列方式可以通过修改HTML元素"li"的样式获得
-   左键小方块使其消失的功能，需要在生成小方块时对每个小方块注册"onclick"事件
-   你可能需要学习一些基础的HTML、JS、CSS的语法，可以考虑参考[这个网站](https://www.runoob.com/ "这个网站")上的教程

# 6. 204什么情况？- Backend

根据传统，老东西们会在204门口的电闸上贴上一个便利贴，写着“都别动！动会死！”。因为协会在自闭间有两台服务器，其中一台服务器上运行着EVA File、EVA Wiki等各种重要的网络服务。如果断电了，协会的部分网络服务无法正常运行，也有可能会损伤服务器硬件。但是，如果碰到了突发情况，又没有人在204，如何方便得知204服务器运行的状态呢？

我们可以在另一台较可靠的服务器（如云服务器）上搭建一个简单的服务端，并让204的服务器定时向云服务器发送请求。如果云服务器那端验证是204服务器发送过来的请求，那么就更新时间戳。如果过了一定时间（如：5分钟）没有来自204服务器的请求，那么就说明204的服务器可能出了问题，需要去检查情况了。

现在，就请你根据以上描述搭建一个简单的云端监测服务吧！

### 你的任务

在本地搭建一个服务端，并完成以下需求：

| **需求**  | **需求描述**                            | **HTTP路由**      |
| ------- | ----------------------------------- | --------------- |
| 检验网络连通性 | 服务端在接收到客户端发来请求后返回成功提示               | `[GET]` /ping   |
| 更新时间戳   | 服务端在接收到待检测服务器发送的请求后返回更新情况           | `[POST]`/check  |
| 查询更新时间  | 服务端在接收到客户端发送的服务器名称后返回上一次更新时间、以及是否失联 | `[POST]`/status |

-   **通用格式**：
    所有的返回值都需要符合以下格式：
    ```json
    {
        "code": 0,       // 错误码，非 0 表示失败
        "msg": "",       // 错误描述
        "data":
          {
              ...        // 数据主体中的具体内容
          },
    }
    ```

在后续的返回值格式描述中，我们将**略去通用部分**，只描述数据主体（即 data 部分）的格式。

-   `[GET]`/ping
    **返回值**：
    ```json
    {
        "msg": "pong"
    }
    ```
    
-   `[POST]`/check
    **请求体**：
    ```json
    {
        "source": "ZJUEVA204", // 待监测服务器名称，你可以自拟
    }

    ```
    **返回体**：
    ```json
    {
        "isChecked": true,     // 是否更新时间戳
    }
    ```
    **可能的错误信息**：
    1.  服务器未在监测列表内
        ```json
        {
            "code": 100,
            "msg": "Server not authorized",
            "data":
              {
                  "isChecked": false,
                  ...
              },
        }
        ```
    2.  请求未提供服务器名称
        ```json
        {
            "code": 101,
            "msg": "Should have a server name",
            ...
        }
        ```
    
-   `[POST]`/status
    **请求体**：
    ```json
    {
        "server": "ZJUEVA204", // 待检查服务器名称，你可以自拟
    }
    ```
    **返回体**：
    ```json
    {
        "lastTime": "1984/10/01 11:45:14", // 按照"yyyy/mm/dd hh:mm:ss"的格式返回
        "isDisconnected": true,            // 是否失联
        "hitokoto": "你好呀，祝你满绩每一天"  // （选做）一言 API 返回的语句
    }
    ```
    不要求错误处理。

    你可以适当减少监测间隔时长以方便调试。

### 附加题

- 将服务器名称链接数据库，形式不限，数据自拟即可。
- 当访问`/status`路由时从一言API拉取语句数据再发送给客户端，你可能要再自拟一些错误情况处理。

### 一些需要注意的事

本题不做语言限制，你可以用任何你喜欢的编程语言完成这一题。如果你没有头绪的话，可以试试
Go的Gin框架，或者Nodejs的express包，Python与C#也是一个不错的选择。~~C++也不是不行~~
服务只需要在本地运行，不必上传至服务器，提交时提交源码即可。

### 参考资料

-   [Nodejs](https://nodejs.org/en/docs/ "Nodejs")
-   [Express](https://expressjs.com/ "Express")
-   [The Little Go Book](https://www.openmymind.net/assets/go/go.pdf "The Little Go Book")
-   [Gin](https://gin-gonic.com/ "Gin")
-   [What is Hypertext Transfer Protocol (HTTP)](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol "What is Hypertext Transfer Protocol (HTTP)")
-   [HTTP Tutorial](https://www.runoob.com/http/http-tutorial.html "HTTP Tutorial")

# 7. 反思 - Encryption

在第0题中，我们给你提供了一个被加密过的字符串，密钥是你的学号后4位，这是我们双方都知道的。那你有没有想过使用这种加密方式可能产生的问题？

假设我们要送一些秘密货物给你，我们把它们锁在了几个箱子里，但是你事先没有钥匙，因此我们也要把钥匙寄给你。如果这个时候送信人把钥匙拿来开锁，或者直接复制一份钥匙以便于打开接下来的货物，这是很危险的。但这个时候我们想出了一个好办法。

我们让你准备了一堆相同的锁和钥匙，然后让邮差把这一堆锁寄给我们。以后每次我们要送秘密货物的时候，我们就用这个锁把箱子锁上，寄给你之后你也能用自己的钥匙打开。这个过程从头到尾邮差都不会和钥匙有任何接触，因此是比较安全的。

这就是对称加密和非对称加密的简单解释。

### 你的任务

1.  整理出对称加密和非对称加密算法各2个，总结他们的特点；
2.  回答问题：为什么非对称加密算法的密钥很难破解？密钥的安全性有哪些理论依据？
3.  回答问题：如果让你设计一个非对称加密算法，你会注意哪些方面？

### 附加题

1.  除了对称加密和非对称加密，还有一种加密算法是散列加密，列举它的几个应用场景。
2.  尝试设计出一个简单的非对称加密算法/散列算法。

# X. 喜欢您来 - And You

感谢你看到这里！

如果你有什么觉得足够有趣，并且实现难度等同或超过上述题目的项目，我们也非常欢迎你自行提交项目仓库链接与项目说明。

期待你的表现！
